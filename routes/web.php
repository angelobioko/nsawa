<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Route::get('/report', function(){
// 	return view('report');
// });

Auth::routes();

Route::get('/home', 'HomeController@index');


Route::get('/','ProjectController@welcome');
Route::resource('project','ProjectController');
Route::post('project/update/{id}','ProjectController@update');

Route::resource('record','RecordController');
Route::post('record/update/{id}','RecordController@update');
Route::get('record/print/{id}','RecordController@print');
Route::get('/report','RecordController@report');