<div class="modal-content">
        <form action="{{url('project/update').'/'.$project->id}}" method="POST" class="horizontal-form" enctype="multipart/form-data">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Project -> {{$project->name}}</h4>
            </div>
            <div class="modal-body">
            	{{csrf_field()}}              
                <div class="form-body">
                	<div class="row">
	                	<div class="col-md-6">
	                		<h3 class="form-section">Client Info</h3>
		                    <div class="form-group">
		                       <label class="control-label col-md-3">Name</label>
		                       <div class="col-md-9">
		                           <input type="text" class="form-control" name="name" id="name" placeholder="" value="{{$project->name}}" required="">
		                           <span class="help-block"><span style="color: white">-</span></span>
		                       </div>
		                    </div>
		                    <div class="form-group">
		                       <label class="control-label col-md-3">Telephone</label>
		                       <div class="col-md-9">
		                           <input type="text" class="form-control" name="telephone" id="mask_number" value="{{$project->telephone}}" placeholder="">
		                           <span class="help-block"><span style="color: white">-</span></span>
		                       </div>
		                    </div>
		                    <div class="form-group">
		                       <label class="control-label col-md-3">E-mail</label>
		                       <div class="col-md-9">
		                           <input type="email" class="form-control" name="email" id="email" value="{{$project->email}}" placeholder="">
		                           <span class="help-block"><span style="color: white">-</span></span>
		                       </div>
		                    </div>
		                    
                		</div>
	                	<div class="col-md-6">
		                    <h3 class="form-section">Messaging Info</h3>  
		                    <div class="form-group">
		                       <label class="control-label col-md-3">Sender ID</label>
		                       <div class="col-md-9">
		                       	   <input type="text" class="form-control" maxlength="11" name="senderid" id="maxlength_defaultconfig" placeholder="This textbox has 11 chars limit." value="{{$project->senderid}}" >		   
		                           <span class="help-block"><span style="color: white">-</span></span>
		                       </div>
		                    </div>
		                    <div class="form-group">
		                       <label class="control-label col-md-3">Message</label>
	                            <div class="col-md-9">
	                                <textarea id="maxlength_textarea" class="form-control" name="message" maxlength="225" rows="4" placeholder="This textarea has a limit of 225 chars." style="resize: none;">{{$project->message}}</textarea>
		                           <span class="help-block"><span style="color: white">-</span></span>
	                            </div>
		                    </div>
		                 </div>
                    </div>
                
                <hr>
                <div class="row">
                	<h3 style="margin-left: 15px">Images Info</h3>
                	<div class="col-md-6">                			 
		                    <div class="form-group">
		                       <label class="control-label col-md-3">Icon</label>
		                       <div class="col-md-9">
			                       	<div class="fileinput fileinput-new" data-provides="fileinput">
	                                    <div class="input-group input-large">
	                                        <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput">
	                                            <i class="fa fa-file "></i>&nbsp;
	                                            <span class="fileinput-filename"> </span>
	                                        </div>
	                                        <span class="input-group-addon btn default btn-file">
	                                            <span class="fileinput-new"> Select file </span>
	                                            <span class="fileinput-exists"> Change </span>
	                                            <input type="file" name="icon"> </span>
	                                    </div>
	                                </div>
		                           <span class="help-block"><span style="color: white">-</span></span>
		                       </div>
		                    </div>
		                     <div class="form-group">
		                       <label class="control-label col-md-3">Bg Image</label>
		                       <div class="col-md-9">
		                       	   <div class="fileinput fileinput-new" data-provides="fileinput">
	                                    <div class="input-group input-large">
	                                        <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput">
	                                            <i class="fa fa-file "></i>&nbsp;
	                                            <span class="fileinput-filename"> </span>
	                                        </div>
	                                        <span class="input-group-addon btn default btn-file">
	                                            <span class="fileinput-new"> Select file </span>
	                                            <span class="fileinput-exists"> Change </span>
	                                            <input type="file" name="bgimage"> </span>
	                                    </div>
	                                </div>	   
		                           <span class="help-block"><span style="color: white">-</span></span>
		                       </div>
		                    </div>		                    
                	</div>
                	<div class="col-md-6">
                		<div class="form-group">
		                       <label class="control-label col-md-12">Slider Images</label>
		                       <div class="col-md-9">
		                       	   <div class="fileinput fileinput-new" data-provides="fileinput">
	                                    <div class="input-group input-large">
	                                        <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput">
	                                            <i class="fa fa-file "></i>&nbsp;
	                                            <span class="fileinput-filename"> </span>
	                                        </div>
	                                        <span class="input-group-addon btn default btn-file">
	                                            <span class="fileinput-new"> Select file </span>
	                                            <span class="fileinput-exists"> Change </span>
	                                            <input type="file" name="sliderimg[]" multiple=""> </span>
	                                    </div>
	                                </div>	   
		                           <span class="help-block"><span style="color: white">-</span></span>
		                       </div>
		                    </div>		  
                	</div>                	
                </div>
              </div> 
            </div>
            <div class="modal-footer">
                <button type="reset" data-dismiss="modal" class="btn dark btn-outline">Cancel</button>
                <button type="submit" class="btn green">Submit</button>
            </div>
            </form>
        </div> 