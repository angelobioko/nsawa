@extends('layouts.admin')

@section('styles')
 <link href="{{url('/')}}/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
 <link href="{{url('/')}}/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<link href="{{url('/')}}/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />

@stop

@section('content')

	<a class=" btn purple btn-outline sbold" data-toggle="modal" href="#static"> Add Project </a>
	<table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_2">
	    <thead>
	        <tr>
	            <th> Id</th>
	            <th> Name </th>
	            <th> Email </th>
	            <th> Telephone </th>
	            <th> Sms SenderID </th>
	            <th> Status </th>
	            <th> Action</th>
	        </tr>
	    </thead>
	    <tbody>
	    @foreach($projects as $project)
	        <tr class="odd gradeX">
	            <td>{{$project->id}}</td>
	            <td>{{$project->name}}</td>
	            <td>{{$project->email}}</td>
	            <td>{{$project->telephone}}</td>
	            <td>{{$project->senderid}}</td>
	            <td>
	                <span class="label label-sm label-success">{{$project->status}}</span>
	            </td>
	            <td>
	            	<a href="#" class="btn btn-xs btn-primary" role="button" data-toggle="modal" data-link="{{url('/project').'/'.$project->id.'/edit'}}" data-target="#edit"><span class="fa fa-edit"></span></a>
	            	<a href="" class="btn btn-xs btn-warning" >b</a>
	            	<a href="" class="btn btn-xs btn-success">c</a>
	            </td>
	        </tr>
	       @endforeach	      
	    </tbody>
	</table>
	
	<div id="static" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        <form action="{{url('project')}}" method="POST" class="horizontal-form" enctype="multipart/form-data">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">New Project</h4>
            </div>
            <div class="modal-body">
            	{{csrf_field()}}              
                <div class="form-body">
                	<div class="row">
	                	<div class="col-md-6">
	                		<h3 class="form-section">Client Info</h3>
		                    <div class="form-group">
		                       <label class="control-label col-md-3">Name</label>
		                       <div class="col-md-9">
		                           <input type="text" class="form-control" name="name" id="name" placeholder="" required="">
		                           <span class="help-block"><span style="color: white">-</span></span>
		                       </div>
		                    </div>
		                    <div class="form-group">
		                       <label class="control-label col-md-3">Telephone</label>
		                       <div class="col-md-9">
		                           <input type="text" class="form-control" name="telephone" id="mask_number" placeholder="">
		                           <span class="help-block"><span style="color: white">-</span></span>
		                       </div>
		                    </div>
		                    <div class="form-group">
		                       <label class="control-label col-md-3">E-mail</label>
		                       <div class="col-md-9">
		                           <input type="email" class="form-control" name="email" id="email" placeholder="">
		                           <span class="help-block"><span style="color: white">-</span></span>
		                       </div>
		                    </div>
		                    
                		</div>
	                	<div class="col-md-6">
		                    <h3 class="form-section">Messaging Info</h3>  
		                    <div class="form-group">
		                       <label class="control-label col-md-3">Sender ID</label>
		                       <div class="col-md-9">
		                       	   <input type="text" class="form-control" maxlength="11" name="senderid" id="maxlength_defaultconfig" placeholder="This textbox has 11 chars limit." >		   
		                           <span class="help-block"><span style="color: white">-</span></span>
		                       </div>
		                    </div>
		                    <div class="form-group">
		                       <label class="control-label col-md-3">Message</label>
	                            <div class="col-md-9">
	                                <textarea id="maxlength_textarea" class="form-control" name="message" maxlength="225" rows="4" placeholder="This textarea has a limit of 225 chars." style="resize: none;"></textarea>
		                           <span class="help-block"><span style="color: white">-</span></span>
	                            </div>
		                    </div>
		                 </div>
                    </div>
                
                <hr>
                <div class="row">
                	<h3 style="margin-left: 15px">Images Info</h3>
                	<div class="col-md-6">                			 
		                    <div class="form-group">
		                       <label class="control-label col-md-3">Icon</label>
		                       <div class="col-md-9">
			                       	<div class="fileinput fileinput-new" data-provides="fileinput">
	                                    <div class="input-group input-large">
	                                        <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput">
	                                            <i class="fa fa-file "></i>&nbsp;
	                                            <span class="fileinput-filename"> </span>
	                                        </div>
	                                        <span class="input-group-addon btn default btn-file">
	                                            <span class="fileinput-new"> Select file </span>
	                                            <span class="fileinput-exists"> Change </span>
	                                            <input type="file" name="icon"> </span>
	                                    </div>
	                                </div>
		                           <span class="help-block"><span style="color: white">-</span></span>
		                       </div>
		                    </div>
		                     <div class="form-group">
		                       <label class="control-label col-md-3">Bg Image</label>
		                       <div class="col-md-9">
		                       	   <div class="fileinput fileinput-new" data-provides="fileinput">
	                                    <div class="input-group input-large">
	                                        <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput">
	                                            <i class="fa fa-file "></i>&nbsp;
	                                            <span class="fileinput-filename"> </span>
	                                        </div>
	                                        <span class="input-group-addon btn default btn-file">
	                                            <span class="fileinput-new"> Select file </span>
	                                            <span class="fileinput-exists"> Change </span>
	                                            <input type="file" name="bgimage"> </span>
	                                    </div>
	                                </div>	   
		                           <span class="help-block"><span style="color: white">-</span></span>
		                       </div>
		                    </div>		                    
                	</div>
                	<div class="col-md-6">
                		<div class="form-group">
		                       <label class="control-label col-md-12">Slider Images</label>
		                       <div class="col-md-9">
		                       	   <div class="fileinput fileinput-new" data-provides="fileinput">
	                                    <div class="input-group input-large">
	                                        <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput">
	                                            <i class="fa fa-file "></i>&nbsp;
	                                            <span class="fileinput-filename"> </span>
	                                        </div>
	                                        <span class="input-group-addon btn default btn-file">
	                                            <span class="fileinput-new"> Select file </span>
	                                            <span class="fileinput-exists"> Change </span>
	                                            <input type="file" name="sliderimg[]" multiple=""> </span>
	                                    </div>
	                                </div>	   
		                           <span class="help-block"><span style="color: white">-</span></span>
		                       </div>
		                    </div>		  
                	</div>                	
                </div>
              </div> 
            </div>
            <div class="modal-footer">
                <button type="reset" data-dismiss="modal" class="btn dark btn-outline">Cancel</button>
                <button type="submit" class="btn green">Submit</button>
            </div>
            </form>
        </div>        
    </div>
</div>

<div id="edit" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg edit">
               
    </div>
</div>
@stop

@section('scripts')
<script src="{{url('/')}}/assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="{{url('/')}}/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="{{url('/')}}/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script src="{{url('/')}}/assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>
<script src="{{url('/')}}/assets/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
<script src="{{url('/')}}/assets/pages/scripts/components-bootstrap-maxlength.min.js" type="text/javascript"></script>

<script src="{{url('/')}}/assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js" type="text/javascript"></script>
<script src="{{url('/')}}/assets/pages/scripts/form-input-mask.min.js" type="text/javascript"></script>

<script src="{{url('/')}}/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
<script type="text/javascript">
	$('#edit').on('show.bs.modal', function (e) {
               var edit = $(e.relatedTarget); 
               var path = edit.attr('data-link'); 
               //console.log(path);
               $( ".edit" ).load(path);
     });
</script>

 
@stop