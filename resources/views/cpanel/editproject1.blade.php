@extends('layouts.admin')

@section('styles')
 <link href="{{url('/')}}/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
 <link href="{{url('/')}}/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<link href="{{url('/')}}/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
<link href="{{url('/')}}/assets/datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />



@stop

@section('content')

<!-- BEGIN FORM-->
	<form action="#" class="horizontal-form">
	    <div class="form-body">
	    	<div class="row">
	                	<div class="col-md-6">
	                		<h3 class="form-section">Client Info</h3>
		                    <div class="form-group">
		                       <label class="control-label col-md-3">Name</label>
		                       <div class="col-md-9">
		                           <input type="text" class="form-control" name="name" id="name" value="{{$project->name}}" placeholder="" required="">
		                           <span class="help-block"><span style="color: white">-</span></span>
		                       </div>
		                    </div>
		                    <div class="form-group">
		                       <label class="control-label col-md-3">Telephone</label>
		                       <div class="col-md-9">
		                           <input type="text" class="form-control" name="telephone" id="mask_number" value="{{$project->telephone}}" placeholder="">
		                           <span class="help-block"><span style="color: white">-</span></span>
		                       </div>
		                    </div>
		                    <div class="form-group">
		                       <label class="control-label col-md-3">E-mail</label>
		                       <div class="col-md-9">
		                           <input type="email" class="form-control" name="email" id="email" value="{{$project->email}}" placeholder="">
		                           <span class="help-block"><span style="color: white">-</span></span>
		                       </div>
		                    </div>
		                    
                		</div>
	                	<div class="col-md-6">
		                    <h3 class="form-section">Messaging Info</h3>  
		                    <div class="form-group">
		                       <label class="control-label col-md-3">Sender ID</label>
		                       <div class="col-md-9">
		                       	   <input type="text" class="form-control" maxlength="11" name="senderid" id="maxlength_defaultconfig" value="{{$project->senderid}}">		   
		                           <span class="help-block"><span style="color: white">-</span></span>
		                       </div>
		                    </div>		                    
		                    <div class="form-group">
		                       <label class="control-label col-md-3">Message</label>
	                            <div class="col-md-9">
	                                <textarea id="maxlength_textarea" class="form-control" name="message" maxlength="225" rows="4" value="{{$project->message}}" style="resize: none;"></textarea>
		                           <span class="help-block"><span style="color: white">-</span></span>
	                            </div>
		                    </div>
		                    <div class="form-group">
		                       <label class="control-label col-md-3">Schedule Date</label>
		                       <div class="col-md-9">
		                       	   <div class="input-group date form_datetime">
                                       <input type="text" size="16" readonly class="form-control">
                                       <span class="input-group-btn">
                                       	<i class="fa fa-calendar"></i>
                                           {{-- <button class="btn default date-set" type="button">
                                               
                                           </button> --}}
                                       </span>
                                   </div>	   
		                           <span class="help-block"><span style="color: white">-</span></span>
		                       </div>
		                    </div>
		                   {{--  <div class="form-group">
		                        <label class="control-label col-md-3">Default Datetimepicker</label>
		                        <div class="col-md-4">
		                            <div class="input-group date form_datetime">
		                                <input type="text" size="16" readonly class="form-control">
		                                <span class="input-group-btn">
		                                    <button class="btn default date-set" type="button">
		                                        <i class="fa fa-calendar"></i>
		                                    </button>
		                                </span>
		                            </div>
		                        </div>
		                    </div> --}}
		                 </div>
                    </div>
                
                <hr>
                <div class="row">
                	<h3 style="margin-left: 15px">Images Info</h3>
                	<div class="col-md-6">                			 
		                    <div class="form-group">
		                       <label class="control-label col-md-3">Icon</label>
		                       <div class="col-md-9">
			                       	<div class="fileinput fileinput-new" data-provides="fileinput">
	                                    <div class="input-group input-large">
	                                        <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput">
	                                            <i class="fa fa-file "></i>&nbsp;
	                                            <span class="fileinput-filename"> </span>
	                                        </div>
	                                        <span class="input-group-addon btn default btn-file">
	                                            <span class="fileinput-new"> Select file </span>
	                                            <span class="fileinput-exists"> Change </span>
	                                            <input type="file" name="icon" > </span>
	                                    </div>
	                                </div>
		                           <span class="help-block"><span style="color: white">-</span></span>
		                       </div>
		                    </div>
		                     <div class="form-group">
		                       <label class="control-label col-md-3">Bg Image</label>
		                       <div class="col-md-9">
		                       	   <div class="fileinput fileinput-new" data-provides="fileinput">
	                                    <div class="input-group input-large">
	                                        <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput">
	                                            <i class="fa fa-file "></i>&nbsp;
	                                            <span class="fileinput-filename"> </span>
	                                        </div>
	                                        <span class="input-group-addon btn default btn-file">
	                                            <span class="fileinput-new"> Select file </span>
	                                            <span class="fileinput-exists"> Change </span>
	                                            <input type="file" name="bgimage"> </span>
	                                    </div>
	                                </div>	   
		                           <span class="help-block"><span style="color: white">-</span></span>
		                       </div>
		                    </div>		                    
                	</div>
                	<div class="col-md-6">
                		<div class="form-group">
		                       <label class="control-label col-md-12">Slider Images</label>
		                       <div class="col-md-9">
		                       	   <div class="fileinput fileinput-new" data-provides="fileinput">
	                                    <div class="input-group input-large">
	                                        <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput">
	                                            <i class="fa fa-file "></i>&nbsp;
	                                            <span class="fileinput-filename"> </span>
	                                        </div>
	                                        <span class="input-group-addon btn default btn-file">
	                                            <span class="fileinput-new"> Select file </span>
	                                            <span class="fileinput-exists"> Change </span>
	                                            <input type="file" name="sliderimg[]" multiple=""> </span>
	                                    </div>
	                                </div>	   
		                           <span class="help-block"><span style="color: white">-</span></span>
		                       </div>
		                    </div>		  
                	</div>                	
                </div>
	    </div>
	</form>
	<!-- END FORM-->
	@stop

@section('scripts')
<script src="{{url('/')}}/assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="{{url('/')}}/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="{{url('/')}}/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script src="{{url('/')}}/assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>
<script src="{{url('/')}}/assets/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
<script src="{{url('/')}}/assets/pages/scripts/components-bootstrap-maxlength.min.js" type="text/javascript"></script>

<script src="{{url('/')}}/assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js" type="text/javascript"></script>
<script src="{{url('/')}}/assets/pages/scripts/form-input-mask.min.js" type="text/javascript"></script>

<script src="{{url('/')}}/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>

<script src="{{url('/')}}/assets/datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>

<script type="text/javascript">
    $(".form_datetime").datetimepicker({
        format: "dd MM yyyy - hh:ii"
    });
</script>


 
@stop