@extends('layouts.master')

@section('pagetitle')
New Donation
@endsection

@section('styles')
<link href="{{url('/')}}/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="{{url('/')}}/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<link href="{{url('/')}}/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />

    <style type="text/css">
        .divshadow{
            padding-left: 15px;
            padding-right: 15px;
            padding-bottom: 15px;
            padding-top: 5px;
            border-radius: 0px;
            box-shadow: rgba(0, 0, 0, 0.6) 0px 1px 4px 0px;
            border: 2px solid rgb(255, 255, 255);
            border-image-source: initial;
            border-image-slice: initial;
            border-image-width: initial;
            border-image-outset: initial;
            border-image-repeat: initial;
            background-color: rgba(255,255,255, 0.5);   
            background-repeat: repeat-x;
            background-position: 0px 0px;
            height: auto;
            }

        .leftdiv{
            padding-right: 0px !important;
            padding-left: 0px !important;
            background-color: rgb(60, 178, 178);
            border-radius: 0px;
            box-shadow: rgba(0, 0, 0, 0.6) 0px 1px 4px 0px;
            border: 2px solid rgb(255, 255, 255);
            border-image-source: initial;
            border-image-slice: initial;
            border-image-width: initial;
            border-image-outset: initial;
            border-image-repeat: initial;
            /* background-image: url(https://storage.googleapis.com/static.parastorage.com/services/skins/2.833.9/images/wysiwyg/core/themes/base/apple_box.png); */
            background-repeat: repeat-x;
            background-position: 0px 0px;
            }
    </style>
@endsection

@section('content-title')

@endsection

@section('content')
    <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper"">
                    <!-- BEGIN CONTENT BODY --> 
                    <div class="col-md-12">
                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <div class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption font-dark">
                                    <i class="icon-settings font-dark"></i>
                                    <span class="caption-subject bold uppercase">Records</span>
                                </div>
                                <div class="tools"> </div>
                            </div>
                            <div class="portlet-body">
                                <table class="table table-striped table-bordered table-hover" id="sample_1">
                                    <thead>
                                        <tr>
                                            <th>id</th>
                                            <th>Name</th>
                                            <th>Amount</th>
                                            <th>Telephone</th>
                                            <th>Reciever</th>
                                            <th>Comment</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>                                    
                                    <tbody>
                                    @foreach($records as $record)
                                        <tr>
                                            <td>{{$record->id}}</td>
                                            <td>{{$record->name}}</td>
                                            <td>{{$record->currency}} {{$record->amount}}</td>
                                            <td>{{$record->telephone}}</td>
                                            <td>{{$record->receiver}}</td>
                                            <td>{{$record->comment}}</td>
                                            <td>
                                                <a href="{{url('record/print').'/'.$record->id}}" class="btn btn-sm btn-primary">Print</a>
                                                <a href="#" class="btn btn-sm btn-warning" role="button" data-toggle="modal" data-link="{{url('record').'/'.$record->id.'/edit'}}" data-target="#edit">Edit</a>
                                                <a href="#" class="btn btn-sm btn-danger" role="button" data-toggle="modal" data-target="#delete" data-id="">Send SMS</a>
                                            </td>
                                        </tr>
                                        @endforeach                                       
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- END EXAMPLE TABLE PORTLET-->
                    </div>
                    <!-- END CONTENT BODY -->
                </div>
                <!-- END CONTENT -->     

                <!-- edit more Modal -->
                <div class="modal fade bs-example-modal-lg" id="edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Edit Record</h4>
                      </div>
                      <div class="modal-body edit">                        
                    
                      </div>
                      
                    </div>
                  </div>
                </div>    

                 <!-- Print Modal -->
                {{-- <div class="modal fade" id="print" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <h4 class="modal-title" id="myModalLabel">Delete Modal</h4>
                        </div>
                        <form method="POST" action="{{url('record/print').'/'.$record->id}}" class="wizard-big" >
                          <div class="modal-body">
                          
                         
                                 <input type="hidden" name="_token" value="{{csrf_token()}}">
                                 <input type="hidden" name="id" value="" class="deletes">

                                  <h4>Are you sure you want to print? </h4>
                           
                            </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary" type="submit">Print</button>
                        </div> 
                        </form>      
                      </div>      
                    </div>
                  </div>        --}}
@endsection

@section('script')
 <script src="{{url('/')}}/assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="{{url('/')}}/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="{{url('/')}}/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script src="{{url('/')}}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="{{url('/')}}/assets/pages/scripts/table-datatables-buttons.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $('#edit').on('show.bs.modal', function (e) {
               var edit = $(e.relatedTarget); 
               var path = edit.attr('data-link'); 
               console.log(path);
               $( ".edit" ).load(path);
     });
</script>
@endsection