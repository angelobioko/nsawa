@extends('layouts.master')

@section('pagetitle')
New Donation
@endsection

@section('styles')
<link href="{{url('/')}}/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
<link href="{{url('/')}}/assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
<link href="{{url('/')}}/assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
<link href="{{url('/')}}/assets/global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css" />

    <style type="text/css">
        .desc{
            padding-top: 30px !important;
        }
        .number{
            padding-top: 0px !important;
        }
        .divshadow{
            padding-left: 15px;
            padding-right: 15px;
            padding-bottom: 15px;
            padding-top: 5px;
            border-radius: 0px;
            box-shadow: rgba(0, 0, 0, 0.6) 0px 1px 4px 0px;
            border: 2px solid rgb(255, 255, 255);
            border-image-source: initial;
            border-image-slice: initial;
            border-image-width: initial;
            border-image-outset: initial;
            border-image-repeat: initial;
            background-color: rgba(255,255,255, 0.5);   
            background-repeat: repeat-x;
            background-position: 0px 0px;
            height: auto;
            }

        .leftdiv{
            padding-right: 0px !important;
            padding-left: 0px !important;
            background-color: rgb(60, 178, 178);
            border-radius: 0px;
            box-shadow: rgba(0, 0, 0, 0.6) 0px 1px 4px 0px;
            border: 2px solid rgb(255, 255, 255);
            border-image-source: initial;
            border-image-slice: initial;
            border-image-width: initial;
            border-image-outset: initial;
            border-image-repeat: initial;
            /* background-image: url(https://storage.googleapis.com/static.parastorage.com/services/skins/2.833.9/images/wysiwyg/core/themes/base/apple_box.png); */
            background-repeat: repeat-x;
            background-position: 0px 0px;
            }
    </style>
@endsection

@section('content-title')

@endsection

@section('content')
    
    <!-- BEGIN DASHBOARD STATS 1-->
    <div class="row">
     @foreach($reports as $report)
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-v2 blue" href="#">
                <div class="visual">
                    <i class="fa fa-comments"></i>
                </div>
                <div class="details">
                    <div class="desc">{{$report->currency}}</div>
                    <div class="number">
                        <span data-counter="counterup" data-value="{{$report->total_amount}}">0</span>
                    </div>
                </div>
            </a>
        </div>
        @endforeach
        <!--
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-v2 red" href="#">
                <div class="visual">
                    <i class="fa fa-bar-chart-o"></i>
                </div>
                <div class="details">
                    <div class="desc">USD</div>
                    <div class="number">
                        <span data-counter="counterup" data-value="12,7">0</span>
                    </div>                                        
                </div>
            </a>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-v2 green" href="#">
                <div class="visual">
                    <i class="fa fa-shopping-cart"></i>
                </div>
                <div class="details">
                    <div class="desc">EUR</div>
                    <div class="number">
                        <span data-counter="counterup" data-value="549">0</span>
                    </div>                                        
                </div>
            </a>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-v2 purple" href="#">
                <div class="visual">
                    <i class="fa fa-globe"></i>
                </div>
                <div class="details">
                    <div class="desc">GBP</div>
                    <div class="number">
                        <span data-counter="counterup" data-value="89"></span>
                    </div>                                        
                </div>
            </a>
        </div>-->
    </div>
    <div class="clearfix"></div>
    <!-- END DASHBOARD STATS 1-->  
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN CHART PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-bar-chart font-green-haze"></i>
                            <span class="caption-subject bold uppercase font-green-haze"> Bar Charts</span>
                            <span class="caption-helper">column and line mix</span>
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"> </a>
                            <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                            <a href="javascript:;" class="reload"> </a>
                            <a href="javascript:;" class="fullscreen"> </a>
                            <a href="javascript:;" class="remove"> </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div id="chart_1" class="chart" style="height: 500px;"> </div>
                    </div>
                </div>
                <!-- END CHART PORTLET-->
        </div>
    </div> 
                                 
@endsection

@section('script')
 <script src="{{url('/')}}/assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
<script src="{{url('/')}}/assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
<script src="{{url('/')}}/assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
<script src="{{url('/')}}/assets/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
<script src="{{url('/')}}/assets/global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
<script src="{{url('/')}}/assets/global/plugins/amcharts/amcharts/radar.js" type="text/javascript"></script>
<script src="{{url('/')}}/assets/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
<script src="{{url('/')}}/assets/global/plugins/amcharts/amcharts/themes/patterns.js" type="text/javascript"></script>
<script src="{{url('/')}}/assets/global/plugins/amcharts/amcharts/themes/chalk.js" type="text/javascript"></script>
<script src="{{url('/')}}/assets/global/plugins/amcharts/ammap/ammap.js" type="text/javascript"></script>
<script src="{{url('/')}}/assets/global/plugins/amcharts/ammap/maps/js/worldLow.js" type="text/javascript"></script>
<script src="{{url('/')}}/assets/global/plugins/amcharts/amstockcharts/amstock.js" type="text/javascript"></script>
 {{-- <script src="{{url('/')}}/assets/pages/scripts/charts-amcharts.js" type="text/javascript"></script> --}}
 <script type="text/javascript">
     var ChartsAmcharts=function() {
         var e=function() {
             var e=AmCharts.makeChart("chart_1", {
                 type:"serial", theme:"light", pathToImages:App.getGlobalPluginsPath()+"amcharts/amcharts/images/", autoMargins:!1, marginLeft:30, marginRight:8, marginTop:10, marginBottom:26, fontFamily:"Open Sans", color:"#888", dataProvider:[ {
                     beneficiary: 'ESI', saturday: 23.5, sunday: 40.1
                 }
                 , {
                     beneficiary: 2010, saturday: 26.2, sunday: 22.8
                 }
                 , {
                     beneficiary: 2011, saturday: 30.1, sunday: 23.9
                 }
                 , {
                     beneficiary: 2012, saturday: 29.5, sunday: 25.1
                 }
                 , {
                     beneficiary: 2013, saturday: 30.6, sunday: 27.2, dashLengthLine: 5
                 }
                 , {
                     beneficiary: 2014, saturday: 34.1, sunday: 29.9, dashLengthColumn: 5, alpha: .2, additional: "(projection)"
                 }
                 ], valueAxes:[ {
                     axisAlpha: 0, position: "left"
                 }
                 ], startDuration:1, graphs:[ {
                     alphaField: "alpha", balloonText: "<span style='font-size:13px;'>[[title]] for [[category]]:<b>[[value]]</b> [[additional]]</span>", dashLengthField: "dashLengthColumn", fillAlphas: 1, title: "Saturday Donation", type: "column", valueField: "saturday"
                 }
                 , {
                     balloonText: "<span style='font-size:13px;'>[[title]] for [[category]]:<b>[[value]]</b> [[additional]]</span>", bullet: "round", dashLengthField: "dashLengthLine", lineThickness: 3, bulletSize: 7, bulletBorderAlpha: 1, bulletColor: "#FFFFFF", useLineColorForBulletBorder: !0, bulletBorderThickness: 3, fillAlphas: 0, lineAlpha: 1, title: "Sunday Donation", valueField: "sunday"
                 }
                 ], categoryField:"beneficiary", categoryAxis: {
                     gridPosition: "start", axisAlpha: 0, tickLength: 0
                 }
             }
             );
             $("#chart_1").closest(".portlet").find(".fullscreen").click(function() {
                 e.invalidateSize()
             }
             )
         };
         
         return {
             init:function() {
                 e()
             }
         }
     }

     ();
     jQuery(document).ready(function() {
         ChartsAmcharts.init()
     }
     );

 </script>


@endsection