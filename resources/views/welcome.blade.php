@extends('layouts.master')

@section('pagetitle')
New Donation
@endsection

@section('styles')
    <style type="text/css">
        .divshadow{
            padding-left: 15px;
            padding-right: 15px;
            padding-bottom: 15px;
            padding-top: 5px;
            border-radius: 0px;
            box-shadow: rgba(0, 0, 0, 0.6) 0px 1px 4px 0px;
            border: 2px solid rgb(255, 255, 255);
            border-image-source: initial;
            border-image-slice: initial;
            border-image-width: initial;
            border-image-outset: initial;
            border-image-repeat: initial;
            background-color: rgba(255,255,255, 0.5);   
            background-repeat: repeat-x;
            background-position: 0px 0px;
            height: auto;
            }

        .leftdiv{
            padding-right: 0px !important;
            padding-left: 0px !important;
            background-color: rgb(60, 178, 178);
            border-radius: 0px;
            box-shadow: rgba(0, 0, 0, 0.6) 0px 1px 4px 0px;
            border: 2px solid rgb(255, 255, 255);
            border-image-source: initial;
            border-image-slice: initial;
            border-image-width: initial;
            border-image-outset: initial;
            border-image-repeat: initial;
            /* background-image: url(https://storage.googleapis.com/static.parastorage.com/services/skins/2.833.9/images/wysiwyg/core/themes/base/apple_box.png); */
            background-repeat: repeat-x;
            background-position: 0px 0px;
            }
    </style>
@endsection

@section('content-title')
New Donation
@endsection

@section('content')
    <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper"">
                    <!-- BEGIN CONTENT BODY -->                    
                    <div class="col-md-3">
                     <div class="leftdiv">
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                <div class="item active">
                                    <img src="{{url('/')}}/uploads/sliderimg/slidepic1.jpg" alt="">
                                </div>
                                @foreach($sliders as $slider)
                              <div class="item ">
                                     <img src="{{url('/uploads/sliderimg/'.$slider->project_id.'/'.$slider->name)}}" alt="">
                                 </div>
                                 @endforeach
                                <!-- <div class="item ">
                                     <img src="img/slidepic3.jpg" alt="">
                                 </div>
                                 <div class="item ">
                                    <img src="img/slidepic4.jpg" alt="">
                                </div>-->
                            </div>                       
                        </div>
                        </div>
                    </div>
                    <div class="col-md-9 divshadow">
                        <!-- BEGIN FORM-->
                         <form method="POST" action="{{url('record')}}" class="wizard-big" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            
                                {{-- <h3 class="form-section">Donor Record</h3> --}}
                                <div class="row">
                                <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Name</label>
                                            <input type="text" id="name" name="name" class="form-control" required="">
                                            <span class="help-block"></span>
                                        </div>
                                  
                                        <div class="form-group">
                                            <label class="control-label">In favour of</label>
                                            <input type="text" id="receiver" name="receiver" class="form-control" placeholder="" required="">
                                            <span class="help-block"></span>
                                        </div>
                                                                    
                                        <div class="form-group">
                                            <label class="control-label">Currency</label>
                                            <div class="radio-list">
                                                <label class="radio-inline" style="padding-left: 20px">
                                                    <input type="radio" name="currency" id="optionsRadios1" value="GHS" checked> GHS
                                                </label>                                              
                                                <label class="radio-inline">
                                                    <input type="radio" name="currency" id="optionsRadios2" value="USD"> USD 
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="currency" id="optionsRadios2" value="GBP"> GBP 
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="currency" id="optionsRadios2" value="EUR"> EUR 
                                                </label>
                                            </div>
                                        </div>
                                   
                                        <div class="form-group">
                                            <label class="control-label">Amount</label>
                                            <input type="text" id="amount" name="amount" class="form-control" placeholder="" required="">
                                            <span class="help-block"></span>
                                        </div>
                                   
                                 
                                        <div class="form-group">
                                            <label class="control-label">Telephone</label>
                                            <input type="text" class="form-control" id="telephone" name="telephone" placeholder=""> 
                                        </div>
                                 
                                        <div class="form-group">
                                            <label class="control-label">Comment</label>
                                            <textarea class="form-control" name="comment" style="resize: none;" rows="3"></textarea>
                                        </div>
                                </div>
                                </div>
                                    
                            <div class="row">
                                <div class="col-md-6 form-actions">
                                     <button type="submit" class="btn blue">
                                    <i class="fa fa-check"></i> Donate</button>
                                </div>
                                <div class="col-md-6">
                                    <button type="button" class="btn default pull-right">Cancel</button>
                                </div>
                            </div>
                           
                        </form>
                        <!-- END FORM-->
                        </div>
                    <!-- END CONTENT BODY -->
                </div>
                <!-- END CONTENT -->                
@endsection

@section('script')
@endsection