            <form id="form" method="POST" action="{{url('record/update').'/'.$record->id}}" class="wizard-big" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <div class="">
                                <h3 class="form-section">Donor Record</h3>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Name</label>
                                            <input type="text" id="name" name="name" value="{{$record->name}}" class="form-control" required="">
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">In favour of</label>
                                            <input type="text" id="receiver" name="receiver" value="{{$record->receiver}}" class="form-control" placeholder="" required="">
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Currency</label>
                                            <div class="radio-list">
                                            <?php 
                                                $curr = $record->currency;
                                             ?>
                                            @if($curr=='GHC')
                                                <label class="radio-inline" style="padding-left: 20px">
                                                    <input type="radio" name="currency" id="optionsRadios1" checked value="GHS"> GHS 
                                                </label>
                                            @else
                                                <label class="radio-inline" style="padding-left: 20px">
                                                    <input type="radio" name="currency" id="optionsRadios1"  value="GHC"> GHC 
                                                </label>
                                            @endif                                          
                                             @if($curr=='USD')
                                                <label class="radio-inline">
                                                    <input type="radio" name="currency" id="optionsRadios3" checked value="USD"> USD 
                                                </label>
                                            @else
                                                <label class="radio-inline">
                                                    <input type="radio" name="currency" id="optionsRadios3" value="USD"> USD 
                                                </label>
                                            @endif
                                             @if($curr=='GBP')
                                                <label class="radio-inline">
                                                    <input type="radio" name="currency" id="optionsRadios4" checked value="GBP"> GBP 
                                                </label>
                                            @else
                                                <label class="radio-inline">
                                                    <input type="radio" name="currency" id="optionsRadios4" value="GBP"> GBP 
                                                </label>
                                            @endif
                                             @if($curr=='EUR')
                                                <label class="radio-inline">
                                                    <input type="radio" name="currency" id="optionsRadios5" checked value="EUR"> EUR 
                                                </label>
                                            @else
                                                <label class="radio-inline">
                                                    <input type="radio" name="currency" id="optionsRadios5" value="EUR"> EUR 
                                                </label>
                                            @endif
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Amount</label>
                                            <input type="text" id="amount" name="amount" value="{{$record->amount}}" class="form-control" placeholder="" required="">
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                    <!--/span-->
                                   
                                </div>
                                <!--/row-->
                                <div class="row">
                                 <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Telephone</label>
                                            <input type="text" class="form-control" id="telephone" name="telephone" value="{{$record->telephone}}" placeholder=""> 
                                        </div>
                                    </div>
                                    <!--/span-->                                    
                                    <div class="col-md-6">
                                        <div class="form-group >
                                            <label class="control-label">Comment</label>
                                            <textarea class="form-control" name="comment" style="resize: none;" rows="3">{{$record->comment}}</textarea>
                                        </div>
                                    </div>
                                    <!--/span-->                                    
                                </div>
                                <!--/row-->                                
                            </div>
                            <button type="button" data-dismiss="modal" class="btn default">Cancel</button>
                            <button type="submit" class="btn blue">
                                <i class="fa fa-check"></i> Update</button>

                </form>