<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request as Requester;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Request;
use Session;

use App\Project;
use App\Slider;
use App\Record;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('cpanel.project')->with('projects',Project::all());
    }


    public function welcome()
    {
        $pid = Project::where('status',1)->value('id');
        // $data = ;

        return view('welcome')->with('sliders',Slider::where('project_id',$pid)->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $inputs = Request::all();

        $project = new Project;
        $project->name=$inputs['name'];
        $project->email=$inputs['email'];
        $project->telephone=$inputs['telephone'];
        $project->senderid=$inputs['senderid'];
        $project->message=$inputs['message'];
        $project->status = 0;

        $extArray = ["jpg","jpeg","png"];

        if(Request::hasFile('icon')){
            $ext = Request::file('icon')->getClientOriginalExtension(); 
           if(in_array(strtolower($ext),$extArray)){
                $file_name = $project->name.'icon'.time().'.'.$ext;
                Request::file('icon')->move('uploads/icons/',$file_name);

                $project->icon = $file_name;
           }else{
            Session::flash('failed', 'Icon file uploaded is not an image');
            return redirect()->back();
           }
        }
        if(Request::hasFile('bgimage')){
            $ext = Request::file('bgimage')->getClientOriginalExtension();             

            if(in_array(strtolower($ext),$extArray))
            {
                $file_name = $project->name.'bg'.time().'.'.$ext;
                Request::file('bgimage')->move('uploads/backgrounds/',$file_name);

                $project->bgimage = $file_name;
            }else{                
                Session::flash('failed', 'Bgimage file uploaded is not an image');
                return redirect()->back();
            }
           
        }

        $project-> save();

        if(Request::hasFile('sliderimg')){
            $images = Request::file('sliderimg');
            $count = 1;
            foreach ($images as $img) {
            $ext = $img->getClientOriginalExtension();             

            if(in_array(strtolower($ext),$extArray))
            {
                $file_name = $project->name.'slider'.$count.time().'.'.$ext;
                $img->move('uploads/sliderimg/'.$project->id.'/',$file_name);

                $slider = new Slider;

                $slider->name = $file_name;
                $slider->project_id = $project->id;

                $slider->save();
                $count++;
            }else{                
                Session::flash('failed', 'File'. $count .'uploaded is not an image');
               // return redirect()->back();
            }
        }
           
        }


        return view('cpanel.project')->with('projects',Project::all());


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('cpanel.editproject')->with('project',Project::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $inputs = Request::all();

        $project = Project::find($id);
        $project->name=$inputs['name'];
        $project->email=$inputs['email'];
        $project->telephone=$inputs['telephone'];
        $project->senderid=$inputs['senderid'];
        $project->message=$inputs['message'];
        

        $extArray = ["jpg","jpeg","png"];

        if(Request::hasFile('icon')){
            $ext = Request::file('icon')->getClientOriginalExtension(); 
           if(in_array(strtolower($ext),$extArray)){
                $file_name = $project->name.'icon'.time().'.'.$ext;
                Request::file('icon')->move('uploads/icons/',$file_name);

                $project->icon = $file_name;
           }else{
            Session::flash('failed', 'Icon file uploaded is not an image');
            return redirect()->back();
           }
        }
        if(Request::hasFile('bgimage')){
            $ext = Request::file('bgimage')->getClientOriginalExtension();             

            if(in_array(strtolower($ext),$extArray))
            {
                $file_name = $project->name.'bg'.time().'.'.$ext;
                Request::file('bgimage')->move('uploads/backgrounds/',$file_name);

                $project->bgimage = $file_name;
            }else{                
                Session::flash('failed', 'Bgimage file uploaded is not an image');
                return redirect()->back();
            }
           
        }

        $project-> save();

        if(Request::hasFile('sliderimg')){
            $images = Request::file('sliderimg');
            $count = 1;
            foreach ($images as $img) {
            $ext = $img->getClientOriginalExtension();             

            if(in_array(strtolower($ext),$extArray))
            {
                $file_name = $project->name.'slider'.$count.time().'.'.$ext;
                $img->move('uploads/sliderimg/'.$project->id.'/',$file_name);

                $slider = new Slider;

                $slider->name = $file_name;
                $slider->project_id = $project->id;

                $slider->save();
                $count++;
            }else{                
                Session::flash('failed', 'File'. $count .'uploaded is not an image');
               // return redirect()->back();
            }
        }
           
        }


        // return view('cpanel.project')->with('projects',Project::all());
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
