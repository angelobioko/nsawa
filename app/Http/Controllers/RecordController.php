<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request as Requester;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

use Request;
use Session;
use Log;

use App\Project;
use App\Slider;
use App\Record;

class RecordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $pid = Project::where('status',1)->value('id');

        return view('record')->with('records',Record::where('project_id',$pid)->latest()->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        //
        $inputs = Request::all();

        $record = new Record;
        $record->name=$inputs['name'];
        $record->currency=$inputs['currency'];
        $record->amount=$inputs['amount'];
        $record->comment=$inputs['comment'];
        $record->telephone=$inputs['telephone'];
        $record->receiver=$inputs['receiver'];
        $record->project_id= Project::where('status',1)->value('id');
        $record->save();

        return view('print')->with('records',$record);


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        return view('edit')->with('record',Record::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        //
         $inputs = Request::all();

         $record = Record::find($id);
        $record->name=$inputs['name'];
        $record->currency=$inputs['currency'];
        $record->amount=$inputs['amount'];
        $record->comment=$inputs['comment'];
        $record->telephone=$inputs['telephone'];
        $record->receiver=$inputs['receiver'];
        $record->save();

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

     /**
     * Print the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function print($id)
    {
        //

        return view('print1')->with('records',Record::find($id));
    }


       /**
     * Generate report specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function report()
    {
        $pid = Project::where('status',1)->value('id');

        $sum = DB::table('records')
                ->select('currency', DB::raw('SUM(amount) as total_amount'))
                ->where('project_id',$pid)
                ->groupBy('currency')
                ->get();

        return view('report')->with('reports',$sum);
    }
}
