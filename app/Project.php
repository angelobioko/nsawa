<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends Model
{
    use softDeletes;

    protected $guard=[];

    public function sliderimages(){
    	return $this->hasMany('App\Slider');
    }

    public function records(){
    	return $this->hasMany('App\Record');
    }

}
